# looking at my recipebox as a guideline
# https://docs.djangoproject.com/en/3.1/ref/request-response/
# https://realpython.com/python-f-strings/
# https://stackoverflow.com/questions/4406377/django-request-to-find-previous-referrer

from django.shortcuts import render, HttpResponseRedirect, reverse, redirect
# from django.http import HttpResponseRedirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from bug_app.models import CustomUser, Ticket
from bug_app.forms import AddTicketForm, LoginForm, EditTicketForm


# Create your views here.
@login_required
def index(request):
    tickets = Ticket.objects.all()
    return render(request, 'index.html', {'tickets': tickets})


@login_required
def ticket_detail_view(request, ticket_id):
    ticket = Ticket.objects.filter(id=ticket_id).first()
    # update(ticket_status=ticket_status, assigned_to=user, completed_by=user)
    return render(request, 'ticket_detail.html', {'ticket': ticket})


@login_required
def add_ticket_view(request):
    if request.method == 'POST':
        form = AddTicketForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Ticket.objects.create(
                title=data.get('title'),
                description=data.get('description'),
                filed_by=request.user
            )

            return HttpResponseRedirect(reverse('homepage'))

    form = AddTicketForm()
    return render(request, 'generic_form.html', {'form': form})


@login_required
def assign_view(request, ticket_id):
    if request.method == 'GET':
        ticket = Ticket.objects.get(id=ticket_id)
        ticket.assigned_to = request.user
        ticket.ticket_status = 'INPROGRESS'
        ticket.save()
        return HttpResponseRedirect(f'/ticket/{ticket_id}')

    return render(request, 'ticket_detail.html')


@login_required
def done_view(request, ticket_id):
    ticket = Ticket.objects.filter(id=ticket_id).first()
    ticket.assigned_to = None
    ticket.completed_by = request.user
    ticket.ticket_status = 'DONE'
    ticket.save()

    return HttpResponseRedirect(request.META.get("HTTP_REFERER"))


def invalid_view(request, ticket_id):
    ticket = Ticket.objects.filter(id=ticket_id).first()
    ticket.assigned_to = None
    ticket.completed_by = None
    ticket.ticket_status = 'INVALID'
    ticket.filed_by = request.user
    ticket.save()

    return HttpResponseRedirect(request.META.get("HTTP_REFERER"))


def edit_ticket_view(request, ticket_id):
    editable = Ticket.objects.get(id=ticket_id)
    if request.method == 'POST':
        form = EditTicketForm(request.POST, instance=editable)
        form.save()
        return HttpResponseRedirect(f'/ticket/{ticket_id}')

    form = EditTicketForm(instance=editable)
    return render(request, 'generic_form.html', {'form': form})


@login_required
def user_detail_view(request, username):
    tickets = Ticket.objects.all()
    user = CustomUser.objects.get(username=username)
    return render(request, 'user_detail.html', {'tickets': tickets, 'user': user})


def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = authenticate(
                request, username=data.get('username'), password=data.get('password')
            )
            if user:
                login(request, user)

                return HttpResponseRedirect(reverse('homepage'))

    form = LoginForm()
    return render(request, 'generic_form.html', {'form': form})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('homepage'))
