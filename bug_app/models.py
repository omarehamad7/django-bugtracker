# pulled stuff from my recipebox and customuser
# pulled time_date from newsapp demo
# https://www.geeksforgeeks.org/related_name-django-built-in-field-validation/
# https://www.youtube.com/watch?v=YIJI5U0BWr0
# https://simpleisbetterthancomplex.com/tips/2018/02/10/django-tip-22-designing-better-models.html

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone


# Create your models here.
class CustomUser(AbstractUser):
    display_name = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.username


class Ticket(models.Model):
    # CHOICES
    NEW = 'NEW'
    INPROGRESS = 'INPROGRESS'
    DONE = 'DONE'
    INVALID = 'INVALID'

    STATUS_CHOICES = [
        (NEW, 'New'),
        (INPROGRESS, 'In Progress'),
        (DONE, 'Done'),
        (INVALID, 'Invalid')
    ]

    title = models.CharField(max_length=200)
    time_date = models.DateTimeField(default=timezone.now)
    description = models.TextField()
    ticket_status = models.CharField(max_length=11, choices=STATUS_CHOICES, default=NEW)
    filed_by = models.ForeignKey(CustomUser, related_name='filed_by', on_delete=models.CASCADE)
    assigned_to = models.ForeignKey(CustomUser, related_name='assigned_to', on_delete=models.CASCADE, null=True, blank=True)
    completed_by = models.ForeignKey(CustomUser, related_name='completed_by', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.title
